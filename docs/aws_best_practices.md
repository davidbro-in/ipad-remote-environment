# Mejores prácticas de AWS
## Seguridad
[Este video](https://youtu.be/SMjvtxXOXdU) tiene diferentes consejos sobre cómo gestionar AWS de forma segura.

### Cuenta _root_
- Desde mi punto de vista, el mejor consejo sobre la cuenta _root_ es "perder" la contraseña de la cuenta y asegurar el proceso de recuperación.
  1. [Crear una cuenta de AWS](https://portal.aws.amazon.com/billing/signup#/start/email)
  1. Configurar una contraseña muy compleja.
  1. Configurar _multi-factor authentication_.
  1. No dejar ningún registro de esta contraseña.
  1. Configurar el e-mail de una persona.
  1. Configurar el número de teléfono de una segunda persona.
  1. Habilitar IAM Identity Center y crear una cuenta administradora.
  1. No utilizar más la cuenta _root_.
## Acceso
### IAM Identity Center
Personalizar la URL del portal de acceso de AWS