# Ambientes en FreeRTLS
El sistema se encuentra basado en diferentes ambientes

## GitLab.com
Es un servicio almacenado y gestionado por GitLab.
Provee:
- Alamcenamiento y control de versiones de código fuente.
- Coordinador para integración contínua.
- Registro de paquetes (Python _package index_).
- Almacenamiento del estado de la infraestructura en la nube (utilizado por Terraform).

Su configuración se puede realizar a través de la web, web API y Terraform (probablemente exista algún otro método). En el contexto de este proyecto se ha decidido interactuar con GitLab únicamente utilizando la web.