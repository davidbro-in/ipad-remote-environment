# Sistema de localización FreeRTLS
FreeRTLS es el nombre del trabajo realizado parte de la carrera de especializacion en sistemas embebidos. Se trata de un sistema de localización en intereiores que se desarrolló teniendo en mente la necesidad de rastrear personas y objetos en lugares donde el GPS no está disponible. Específicamente, el proyecto se llevó adelate para la empresa PLAYTRAK Sistemas de Monitoreo, la cual provee soluciones para gestión de casinos. Este tipo de negocio presenta claros desafíos a la hora de intentar conocer con presión la distribución de los recursos (bienes y personas) que hacen posible la operación.

Para hacer posibe la localización, el sistema se basa en puntos de referencia de ubicación conocida y sensores de distancia que miden las distancias relativas a los puntos de referencia. Además, el sistema consta de un algoritmo para calcular la posición en base a las distancias y la posición absoluta de los anclajes de referencia.

Desde el punto de vista tecnológico, el sistema fue desarrollado utilizando:
- ReactJS y Vite para el frontend.
- Amazon Web Services (AWS) IoT Core como plataforma para la implementación del backend.
- Terraform para describir la infraestructura.
- Gitlab para mejorar el flujo de trabajo agilista, gestionar el código fuente, automatizar pruebas y entrega continua.
- Docker para crear entornos reproducibles.
