# Diseño e implementación
En este capítulo se presenta la arquitectura del sistema desplegado y de la configuraón que permite el desarrollo mismo del proyecto. Uno de los requisitos del cliente era el desarrollo en el contexto de DevOps, por lo que la arquitectura del sistema se encuentra integrada desde el desarrollo hasta la operación y no deja de lado los aspectos relativos a la seguridad.

## Arquitectura del sistema
Para el trabajo se implementó una arquitectura basada en la nube que abarca desde el alojamiento del cóodigo fuente hasta el servidor de contenido para la página web y base de datos. Pasando los componentes necesarios para desplegar y configurar la infraestructura.

## Despliegue del sistema
El despliegue del sistema, en este contexto, implica desde la configuración de gitlab.com hasta poner al sistema disponible para los clientes (despliegue propiamente dicho).

El proceso de despliegue se encuentra automatizado y es parte del ciclo de desarrollo, es decir, todos los cambios aprobados a través de _Merge Requests_ se propagan al ambiente de producción. Para el desarrollo de esta funcionalidad se utilizó la herramienta Terraform y el lenguaje HCL. Mediante su uso es posible describir recursos y su relción entre ellos, en numeras plataformas.

## Modelo de datos
Debido al enfoque tomado para la implementación de la gestión de usuarios, los únicos datos que se requiere evaluar, son los datos provenientes de los sensores.

En la primer etapa de FreeRTLS se implementó un modelo de datos abosulutamente simple. Desde el dispositivo se enviaban los datos de distancia desde el sensor a todos los anclajes que hubiera al alcance.

En esta nueva etapa del proyecto se comenzó con la evaluación del problema desde cero. Se concluyeron algunos aspectos fundamentales de los datos:
- Se trata de datos en los que la componente temporal tiene gran importancia.
- Los datos que se guardan representan el vínculo entre nodos (la distancia que los separa).
- Los campos de interés son pocos:
  - Tiempo.
  - Nodo sensor.
  - Nodo anclaje.
  - Distancia que los separa.

Este análisis arrojó algunas posibles implementaciones para el almacenamiento y posterior lectura de los datos. Por un lado, su naturaleza temporal invita a pensar en que una base de datos especializada en este tipo de datos podría brindar ventajas respecto a tiempos de escritura y lectura. A su vez, también se percibe que sería factible almacenear los datos en una base de datos de grafos. Pero, finalmente, debido a la simplicidad de la estructura requerida, así como de los campos necesarios, también es posible pensar en una base de datos documental o una "simple" base de datos relacional.

Desde el punto de vista tecnológico, tal como es esperable, cualquiera de las opciones presentadas era posible de implementar. Además, el proveedor elegido para la implementación (AWS), brinda múltiples servicios de base de datos, entre los que se incluyen:
- AWS Neptune. Base de datos de grafos sin servidor diseñada para una escalabilidad y disponibilidad superiores.
- AWS Timestream. Base de datos de serie temporal rápida, escalable y sin servidor.
- AWS DynamoDB. Servicio de base de datos NoSQL rápido y flexible para el rendimiento de milisegundos de un solo dígito a cualquier escala.
- AWS DocumentDB (compatible con MongoDB). Escale las cargas de trabajo empresariales con facilidad mediante un servicio de base de datos de documentos JSON nativo completamente administrado.

Utilizar una base de datos orientada a grafos tiene la ventaja de acoplarse fácilmente a las necesidades de los datos. En este caso, cada medición sería representada con una nueva relación entre los nodos fuente y destino. Además, en este tipo de base de datos se podría representar el modelo de datos completo.

En contraste, una base de datos orientada a series temporales no brindaría una forma fácil de almacenar datos estáticos (no temporales), pero brinda una excelente forma de almacenar los datos temporales (mediciones).

En resumen, es posible concluir que justamente la gran diferencia está en la forma en la que se guardan las mediciones y los datos estáticos. Por un lado, las bases de datos de series temporales se encuentran completamente enfocadas en el aspecto temporal, las base de datos orientadas a grafos se ajustan fácilmente a dominio del problema, donde tenemos nodos y relaciones natualmente en el problema.

Finalmente, tal como se implementó desde el principio, AWS DynamoDB es la base de datos mejor integrada con AWS IoT Core y esto representa una ventaja desde el punto de vista de los requisitos como la mantenibilidad o facilidad para ponerlo en funcionamiento.

## Ciberseguridad
Durante la primera etapa de este trabajo, el foco estuvo puesto en poder montar la prueba del concepto y no en hacer un desarrollo seguro. Tras tener dicha prueba de concepto, se procedió al análisis para rediseñar lainfraestructura para convertirala en segura, mientras se mantiene la escalabilidad. En otras palabras, se procuró seguir el enfoque DevSecOps.

El análisis de la arquitectura del sistema, se evaluaron cada una de las interfaces para intentar hacer todas seguras. 

El primer punto de contacto entre un humano y el sistema, en el caso de FreeRTLS, es GitLab. Tal como se mencionó anteriormente, GitLab es utilizado para varias cosas y es el primer punto de contacto.
### Riesgos
Desde el punto de vista de la aplicación, el acceso de no autormizado podría derivar en corrupción de datos o visualización de datos privados. Sin embargo, desde el punto de vistade la infraestructura que soporta dicha aplicación, el accesso no autorizado podría  derivar en la pérdiada total del capital digital. Desde la desactivación parcial del servicio, hasta la destrucción o secuestro del código fuente.

Por otro lado, otro riesgo importante podría ser la exposición de código fuente privado frente a usuarios no autorizados. En este caso, se optó por publicar el código _Open Source_ y con licencias permisivas, lo que directamente deja  fuera del análisis esta problemática. La monetización del trabajo podría venir por otros canales como la operación del sistema, la venta de hardware, etcétera. Pero la monetización del código fuente como tal, implicaría demsiados recursos para asegurar que no se haga púbico. Otra foma sería utilizar una licencia que no autorice la utilización del código fuente para generar ingresos de forma directa o incluso incluir sus derivados.

### Herramientas
### OAuth _logins_
Utilizar OAuth en lugar de tokens  evita la necesidad de rotatión, la cual deriva de la única forma de evitar que un _token_ expuesto sea utilizado.

Debido a la naturaleza _Cloud Native_ gran parte del inventario del sistema se encuentra en la nube, por lo que se requiere acceder a ellos via remota y por tanto es importante asegurar el camino.

#### Inventario
Los activos en este contexto de IT son:
- Terminal de trabajo de los desarrolladores.
- Recursos en GitLab:
  - Repositorios git.
  - Almanecenamiento de estado de Terraform.
  - Registro y almanenamieno de paquetes Python.
  - ¿Coordinador de CI/CD?
- Recursos en AWS.
  - GitLab Runner.
  - FreeRTLS Cloud.
  - Identity Center.
- Dispositivos IoT.
- Navegador web de los usuarios.

#### Modelado de amenazas
Basado en el mismo modelo de arquitectura, es posible evaluar todas las entradas y salidas, así como los diferentes canales de comunicación para asegurar que todos se encuenotran implementados de la forma mas segura posible.

De este modelo surgen canales que es necesario asegurar, como lo fueron los _tokens_ de largo plazos utilizados para acceder a GitLab y AWS. Los cuales se reemplazaron por credenciales temporales gestionadas por las respectivas CLIs.

Otro aspecto que surge del modelo de amenazas es la eventual necesidad de acceder a la máquina virtual donde se ejecuta GitLab Runner. En líneas generales, no es necesario acceder a este servidor porque se encuentra configurado desde el repositorio. Sin embargo, es probable que en algún punto se requiera acceder a él para hacer algún mantemiento o revisar algún problema. En el contexto del modelo de amenazas se busca evitar la exposión de una interface tan amplia como sería SSH. Por lo que se recurre a un servicio brindado por AWS (EC2 Instance Connect) que permite acceder a instancias sin la necesidad de utilizar SSH, certificados de larga vida o incluso sin abrir el puerto SSH a internet.

#### Análisis estático de IaC
Como se presentó anteriormente, toda la infraestructura se encuentra descripta como código y por tanto es posible escanear en busca de vulnerabilidades.

#### _Peer review_ de infraestructura
Otra de las ventajas de la gestión de infraestructura como código es la posibilidad de someter a revisión cada uno de los cambios antes de ser aplicados. Por ejemplo, es fácilmente detectable si hay nuevos puertos abiertos en el _firewall_, nuevos usuarios o cambios de permisos, etcétera.