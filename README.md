# FreeRTLS
FreeRTLS es un sistema de localización en intereiores basado en tecnología UWB.

## Componentes
- Sensor de distancia inalámbrico.
- Aplicación para capturar datos y enviarlos a la nube.
- Infraestructura en la nube basada en AWS IoT Core

## Desarrollo

### Prerequisitos
Para trabajar con este proyecto es necsario contar con:

- _Workstation_ con el siguiente software instalado:
  - Docker.
  - VS Code.
  - DevContainer extension.
- Cuenta AWS (puede ocasionar gastos).
  - IAM Identity Center configurado para SSO.
- Placa de desarrollo basada en Qorvo DWM-1001

### Crear una nueva instancia de FreeRTLS
1. Crear y configurar cuenta AWS
   1. Crear una cuenta _root_ en AWS ([Revisar mejores práticas](docs/aws_best_practices.md#cuenta-root)).
   1. Habilitar IAM Identity Center.
   1. Crear una cuenta administradora.
   1. Habilitar MFA.
   1. Crear un grupo para los administradores.
   1. Asignar usuario administrador al grupo.
   1. Crear un conjunto de permisos.
   1. Agregar el grupo de administradores a la cuenta _root_ de AWS.
   1. Seleccionar el grupo de permisos creado anteriormente.

## License

[MIT](https://choosealicense.com/licenses/mit/)
